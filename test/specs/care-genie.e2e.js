const CareGenieApp = require('../pageobjects/CareGenieApp/care-genie.page');
const Auth = require('../pageobjects/Auth.page');

let auth = new Auth();

describe('Care Genie App', () => {
    before(function () {
        auth.open();
        auth.loginAs("Customer Care");
    });

    it('should display a visible search modal', () => {
      CareGenieApp.open();
      expect(CareGenieApp.searchComponent.modal).toBePresent();
      expect(CareGenieApp.searchComponent.title).toHaveText("Customer Lookup");
      expect(CareGenieApp.searchComponent.searchBox).toBePresent();
      expect(CareGenieApp.searchComponent.notepad).toBePresent();
      expect(CareGenieApp.searchComponent.resultsTable).toBePresent();
      expect(CareGenieApp.searchComponent.lastModifiedCasesTitle).toHaveText("Last Modified Cases");
      expect(CareGenieApp.searchComponent.lastModifiedCasesTable).toBePresent();
    });

    it('should display results with valid service id', () => {
      CareGenieApp.open();
      expect(CareGenieApp.searchComponent.resultsTableResults).toHaveText("No results found. Please enter more than one character to search.");
      CareGenieApp.searchComponent.searchBox.setValue("S-7202423");
      CareGenieApp.searchComponent.searchBoxBtn.click();
      browser.pause(5000);
      expect(CareGenieApp.searchComponent.resultsTableResults).toHaveText("Active");
    });
});


