const Page = require('../page');
const Search = require('./components/search.compoment');
const CustomerVerification = require('./components/customer.verification.compoment');

class CareGeniePage extends Page {
    
    get careGenieBase () { return $('div.slds.cCareGenieBase') }
    get searchComponent () { return new Search(this.careGenieBase); }
    get customerVerification () { return new CustomerVerification(this.careGenieBase); }
    get auraError() { return new AuraError(this.careGenieBase); }

    open () {
        return super.open('/c/CareGenie.app');
    }

    search(criteria) {
        this.searchComponent.searchBox.setValue(criteria);
        this.searchComponent.searchButton.click();
    }
}

module.exports = new CareGeniePage();
