const Component = require('../../component');

class Search extends Component {
    get title () { 

      return this.host.$("//div[3]//div[2]//div//div[2]//div[1]//h2//b");
    }
   
    get modal() {  
      return this.host.$('div.slds-modal.slds-fade-in-open.cCustomerSearchModal');
    }

    get searchBox() {  
      return this.host.$('//*[@id="729:0"]');
    }

    get searchBoxBtn() {  
      return this.host.$('/html/body/div[3]/div[2]/div/div[2]/div[1]/div[1]/div/div/div/button');
    }

    get resultsTableResults() {
      return this.host.$('/html/body/div[3]/div[2]/div/div[2]/div[1]/div[2]/table[1]/tbody/tr/td');
    }
    
    // get searchButton() {  
    //   return this.host.$("/html/body/div[3]/div[2]/div/div[2]/div/div[1]/div/div/div/button");
    // }

    get resultsTable() {  
      return this.host.$("/html/body/div[3]/div[2]/div/div[2]/div[1]/div[2]/table[1]");
    }

    get contractSigner(){
      return this.host.$("/html/body/div[3]/div[2]/div/div[2]/div/div[2]/table/tbody/tr/th[1]/div/span");
    }

    get notepad(){
      return this.host.$("/html/body/div[3]/div[2]/div/div[2]/div[2]");
    }

    get lastModifiedCasesTitle(){
      return this.host.$("/html/body/div[3]/div[2]/div/div[2]/div[1]/div[2]/h2/b");
    }

    get lastModifiedCasesTable(){
      return this.host.$("/html/body/div[3]/div[2]/div/div[2]/div[1]/div[2]/table[2]");
    }
}

module.exports = Search;