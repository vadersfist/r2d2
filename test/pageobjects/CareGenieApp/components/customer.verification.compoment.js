const Component = require('../../component');

class CustomerVerification extends Component {
    get title () { 
      return this.host.$('//*[@id="tab--vivint-pinnedCustomerTab"]/div[2]/div/div[1]/h2/b');
    }

    get accountVerifiedButton(){ return this.host.$('//*[@id="tab--vivint-pinnedCustomerTab"]/div[2]/div/div[3]/button[1]'); }

    get accountNotVerifiedButton(){ return this.host.$('//*[@id="tab--vivint-pinnedCustomerTab"]/div[2]/div/div[3]/button[2]'); }
   
}

module.exports = CustomerVerification;