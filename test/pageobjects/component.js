// meant to be used with webcomponents
// see https://webdriver.io/blog/2019/02/22/shadow-dom-support.html

class Component {

    constructor(host) {
      const selectors = [];
      // Crawl back to the browser object, and cache all selectors
      while (host.elementId && host.parent) {
        selectors.push(host.selector);
        host = host.parent;
      }
      selectors.reverse();
      this.selectors_ = selectors;
    }
  
    get host() {
      // Beginning with the browser object, reselect each element
      return this.selectors_.reduce((element, selector) => element.$(selector), browser);
    }
  }
  
  module.exports = Component;