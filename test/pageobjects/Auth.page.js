const Page = require('./page');
const { admin } = require('../fixtures/users');

class Auth extends Page {
    get username () { return $('#username') }
    get password () { return $('#password') }
    get signIn () { return $('#Login') }
    get errorMessages () { return $('#loginError'); }
    
    loginAs(profileName) {
        this.username.setValue(admin.username);
        this.password.setValue(admin.password);
        this.signIn.click(); 
        browser.waitUntil(() => {
            const signInExists = this.signIn.isExisting();
            const errorExists = this.errorMessages.isExisting();
            return !signInExists || errorExists;
        },
        { timoutMsg: 'The "Sign in" button still exists and an error never appeared' });
        let customerCareLoginLink = browser.loginWithUserWithProfile(profileName);
        console.info('Log in as user: ',customerCareLoginLink);
        browser.url(customerCareLoginLink);
    }

    open () {
        return super.open('/');
    }

    close() {
        return super.close();
    }
    
}

module.exports = Auth;