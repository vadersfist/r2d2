# R2D2

## About 
R2D2 is a proof-of-concept project meant to explore the feasibility of automating e2e testing within Salesforce generally and Vivint's org, specifically.

The goal is to answer the following:

Is it possible to have relaible and maintainable automation that:
* Tests all cases in the current regression suite
* Allows tests to be easily added by any team member (including our non-technical members), 
* Can be validated by any team member (including our non-technical members) 
* Can be understood by any team member (including our non-technical members)

## Running this project
1. Download the project.
2. Run:
```
npm install
```
This project is meant to run against a sandbox.

3. Go to test/fixtures/users.js
4. Enter the username and password for an admin user.
5. From the terminal, run
```
npm run test:all
```

